<?php

namespace App\Console;

use App\Console\Commands\GenerateAppRoles;
use App\Console\Commands\GenerateCarOptions;
use App\Console\Commands\ImportAirports;
use App\Console\Commands\ImportCarsData;
use App\Console\Commands\ImportCountries;
use App\Console\Commands\ImportFlights;
use App\Console\Commands\ImportRolesPermissions;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         // Schedule commands
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
