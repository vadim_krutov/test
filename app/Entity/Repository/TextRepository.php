<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 18/08/16
 * Time: 15:59
 */

namespace App\Entity\Repository;

use App\Entity\User;
use App\Services\InfoService;
use Doctrine\ORM\EntityRepository;

class TextRepository extends EntityRepository
{
    public function findByGroup(string $groupBy)
    {
        return $this->createQueryBuilder('text')
            ->where('text.slug LIKE :slug')
            ->setParameters([
                'slug' => '%'.$groupBy.'%'
            ])
            ->getQuery()->getResult();
    }
}