<?php

namespace App\Entity;

use App\Entity\Traits\Hydratable;
use App\Entity\Traits\Slugify;
use App\Entity\Traits\Translatable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Entity\Repository\TextRepository")
 * @ORM\Table(name="text")
 */
class Text
{
    use Hydratable;
    use Translatable;
    use Slugify;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TextTranslation", mappedBy="text", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Text
     */
    public function setSlug($slug)
    {
        $this->slug = $this->slugify($slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add textTranslation
     *
     * @param \App\Entity\TextTranslation $textTranslation
     *
     * @return Text
     */
    public function addTranslation(\App\Entity\TextTranslation $textTranslation)
    {
        $this->translations[] = $textTranslation;
        $textTranslation->setText($this);

        return $this;
    }

    /**
     * Remove textTranslation
     *
     * @param \App\Entity\TextTranslation $textTranslation
     */
    public function removeTextTranslation(\App\Entity\TextTranslation $textTranslation)
    {
        $this->translations->removeElement($textTranslation);
    }

    /**
     * Get textTranslations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }
}