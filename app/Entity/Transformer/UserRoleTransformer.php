<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 17/08/16
 * Time: 17:34
 */

namespace App\Entity\Transformer;


use App\Entity\User;
use App\Entity\UserRole;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;


class UserRoleTransformer extends TransformerAbstract
{
    public function transform(UserRole $userRole)
    {
        return [
            'id' => $userRole->getId(),
            'name' => $userRole->getName(),
        ];
    }
}