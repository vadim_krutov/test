<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 17/08/16
 * Time: 17:34
 */

namespace App\Entity\Transformer;


use App\Entity\User;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;


class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $manager = new Manager();

        return [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'lastname' => $user->getLastname(),
            'mobilePhone' => $user->getMobilePhone(),
            'address' => $user->getAddress(),
            'address2' => $user->getAddress2(),
            'zip' => $user->getZip(),
            'city' => $user->getCity(),
            'country' => $user->getCountry(),
            'birthday' => $user->getBirthday() ? $user->getBirthday()->format('d/m/Y') : null,
            'gender' => $user->getGender(),
            'email' => $user->getEmail(),
            'role' => $manager->createData(new Item($user->getRole(), new UserRoleTransformer()))->toArray(),
        ];
    }
}