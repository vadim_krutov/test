<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 12/12/2016
 * Time: 14:56
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class InfoService extends Facade
{
    /**
     * Return facade accessor.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'InfoService'; }
}