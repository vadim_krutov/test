<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 07/12/2016
 * Time: 17:37
 */

namespace App\Http\Controllers\Admin\Auth;


use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        $this->validateLogin($request);
        $credentials = $request->only(['email', 'password']);

        /** @var User $user */
        if ($user = \EntityManager::getRepository(User::class)->findOneBy(['email' => $credentials['email']])) {
            if (!$user->isAdmin() && !$user->isAgent()) {
                return \Redirect::route('admin::showLoginForm');
            }
        }

        if (\Auth::guard('admin')->attempt($credentials)) {
            return \Redirect::route('admin::index');
        }

        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => \Lang::get('auth.failed'),
            ]);
    }

    /**
     * @return mixed
     */
    public function logout()
    {
        \Auth::logout();
        \Session::flush();

        return redirect()->route('admin::showLoginForm');
    }
}