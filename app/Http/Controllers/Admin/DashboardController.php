<?php

namespace App\Http\Controllers\Admin;


use App\Entity\Career;
use App\Entity\Fleet;
use App\Entity\Page;
use App\Entity\Service;

class DashboardController extends BaseController
{
    protected function getValidationRules($id = null): array
    {
        return [];
    }

    protected function getRepository()
    {
        return;
    }

    protected function setBreadCrumbs($id = null): array
    {
        // TODO: Implement setBreadCrumbs() method.
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard.index');
    }
}
