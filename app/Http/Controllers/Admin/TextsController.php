<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 02/05/2017
 * Time: 16:11
 */

namespace App\Http\Controllers\Admin;


use App\Entity\Text;
use Doctrine\ORM\EntityNotFoundException;
use Illuminate\Http\Request;

class TextsController extends BaseController
{
    /**
     * @param null $id
     * 
     * @return array
     */
    protected function getValidationRules($id = null): array
    {
        return [
            'slug' => 'required|unique:App\Entity\Text,slug,' . $id ?? '',
            'translations.*.description' => 'required',
        ];
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository()
    {
        return \EntityManager::getRepository(Text::class);
    }

    protected function setBreadCrumbs($id = null): array
    {
        return [
            'index' => [
                route('admin::text.index') => 'Texts'
            ],
            'create' => [
                route('admin::text.index') => 'Texts',
                route('admin::text.showForm') => 'Add New'
            ],
            'update' => [
                route('admin::text.index') => 'Texts',
                route('admin::text.showForm', ['textId' => $id]) => 'Edit'
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $texts = $this->getRepository()->findAll();

        return view('admin.texts.index', [
            'texts' => $texts,
            'breadcrumbs' => $this->setBreadCrumbs()['index']
        ]);
    }

    /**
     * @param null $textId
     *
     * @return mixed
     * @throws EntityNotFoundException
     */
    public function showForm($textId = null)
    {
        $text = null;

        if ($textId && (!$text = $this->getRepository()->find($textId))) {
            throw new EntityNotFoundException(sprintf('Page with id %s not found', $textId));
        }

        return view('admin.texts.form', [
            'text' => $text,
            'breadcrumbs' => $text ? $this->setBreadCrumbs($textId)['update'] : $this->setBreadCrumbs()['create']
        ]);
    }


    /**
     * @param Request $request
     * @param null $textId
     *
     * @return mixed
     * @throws EntityNotFoundException
     */
    public function postText(Request $request, $textId = null)
    {
        if (!$textId) {
            $this->validate($request, $this->getValidationRules());

            $text = new Text();
            $text->hydrate($request->all());
            $text->translate($request->all());

            \Session::flash('success', 'Text was successfully created');
            \EntityManager::persist($text);
            \EntityManager::flush();
        } else {
            /** @var Text $text */
            if (!$text = $this->getRepository()->find($textId)) {
                throw new EntityNotFoundException(sprintf('Text with id %s not found', $textId));
            }

            $this->validate($request, $this->getValidationRules($text->getId()));

            $text->hydrate($request->all());
            $text->translate($request->all());

            \Session::flash('success', 'Text was successfully updated');
            \EntityManager::flush($text);
        }

        return redirect()->route('admin::text.showForm', ['text' => $text->getId()]);
    }

    /**
     * @param $textId
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws EntityNotFoundException
     */
    public function deleteText($textId)
    {
        /** @var Text $text */
        $text = $this->getRepository()->find($textId);

        if (!$text) {
            throw new EntityNotFoundException(sprintf('Page with id %s not found', $textId));
        }

        \EntityManager::remove($text);
        \EntityManager::flush();

        \Session::flash('success', 'Text was successfully deleted');

        return redirect()->back();
    }
}