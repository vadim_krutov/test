<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 08/12/2016
 * Time: 14:07
 */

namespace App\Http\Controllers\Admin;


use App\Entity\Repository\UserRepository;
use App\Entity\User;
use App\Entity\Country;
use App\Policies\RoutePolicy;
use App\Services\InfoService;
use App\Services\UserService;
use Doctrine\ORM\EntityNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UsersController extends BaseController
{
    protected function getValidationRules($id = null): array
    {
        return [
            'create' => [
                'name' => 'required',
                'lastname' => 'required',
                'email' => 'required|email|unique:App\Entity\User,email',
                'role' => 'required|exists:App\Entity\UserRole,id',
                'password' => 'required|min:6',
                'passwordConfirm' => 'required|same:password'
            ],
            'update' => [
                'name' => 'required',
                'lastname' => 'required',
                'role' => 'required|exists:App\Entity\UserRole,id',
            ],
            'subscription' => [
                'subscription' => 'required',
            ]
        ];
    }

    /**
     * @return UserRepository
     */
    protected function getRepository()
    {
        return \EntityManager::getRepository(User::class);
    }

    protected function setBreadCrumbs($id = null): array
    {
        return [
            'index' => [
                route('admin::user.index') => 'Users'
            ],
            'create' => [
                route('admin::user.index') => 'Users',
                route('admin::user.showForm') => 'Add New'
            ],
            'update' => [
                route('admin::user.index') => 'Users',
                route('admin::user.showForm', ['id']) => 'Edit'
            ]
        ];
    }

    public function index() 
    {
        $users = $this->getRepository()->findAll();

        return view('admin.users.index', [
            'users' => $users,
            'breadcrumbs' => $this->setBreadCrumbs()['index']
        ]);
    }

    /**
     * @param null $userId
     *
     * @return mixed
     * @throws EntityNotFoundException
     */
    public function showForm($userId = null)
    {
        $user = null;

        if ($userId && (!$user = $this->getRepository()->find($userId))) {
            throw new EntityNotFoundException(sprintf('User with id %s not found', $userId));
        }

        return view('admin.users.form', [
            'roles' => \InfoService::get(InfoService::DATA_TYPE_FORM_ROLES),
            'user' => $userId ? $this->getRepository()->find($userId) : null,
            'breadcrumbs' => $userId ? $this->setBreadCrumbs($userId)['update'] : $this->setBreadCrumbs()['create']
        ]);
    }
    
    /**
     * @param Request $request
     * @param null $userId
     *
     * @return mixed
     * @throws EntityNotFoundException
     */
    public function postUser(Request $request, $userId = null)
    {
        if (!$userId) {
            $this->validate($request, $this->getValidationRules()['create']);

            $user = new User();
            $user->hydrate($request->all());
            \Session::flash('success', 'User was successfully created');
            \EntityManager::persist($user);
            \EntityManager::flush();
        } else {
            $this->validate($request, $this->getValidationRules()['update']);

            /** @var User $user */
            $user = $this->getRepository()->find($userId);

            if (!$user) {
                throw new EntityNotFoundException(sprintf('User with id %s not found', $userId));
            }

            $user->hydrate($request->all());

            \Session::flash('success', 'User was successfully updated');
            \EntityManager::flush($user);
        }

        return redirect()->route('admin::user.showForm', ['userId' => $user->getId()]);
    }

    /**
     * @param $userId
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws EntityNotFoundException
     */
    public function deleteUser($userId)
    {
        /** @var User $user */
        $user = $this->getRepository()->find($userId);

        if (!$user) {
            throw new EntityNotFoundException(sprintf('User with id %s not found', $userId));
        }

        \EntityManager::remove($user);
        \EntityManager::flush();

        \Session::flash('success', 'User was successfully deleted');

        return redirect()->back();
    }
}