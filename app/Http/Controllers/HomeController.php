<?php

namespace App\Http\Controllers;

use App\Entity\Car;
use App\Entity\CarAdvert;
use App\Entity\CarAdvertContacts;
use App\Entity\CarModel;
use App\Entity\CarOption;
use App\Entity\CarOptionCategory;
use App\Entity\Flight;
use App\Entity\Photo;
use App\Entity\Repository\CarAdvertRepository;
use App\Entity\Repository\CarRepository;
use App\Entity\Repository\FlightsRepository;
use App\Entity\Repository\TextRepository;
use App\Entity\Repository\VacancyRepository;
use App\Entity\Text;
use App\Entity\Transformer\CarModelTransformer;
use App\Entity\Vacancy;
use App\Http\Traits\JsonResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Mail\Message;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HomeController extends Controller
{

    public function index()
    {
        return '...';
    }

}
