<?php

function getTextTranslation($slug, $locale = 'en'): string
{
    /** @var \App\Entity\Text $text */
    $text = \EntityManager::getRepository(\App\Entity\Text::class)->findOneBy(['slug' => $slug]);

    return $text ? $text->getTranslationByLocale($locale)->getDescription() : 'No Translation';
}
