<?php

namespace App\Providers;

use App\Services\InfoService;
use App\Services\MailchimpService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('InfoService', function () {
            return new InfoService();
        });
        
        require_once __DIR__ . '/../Http/helpers.php';
    }
}
