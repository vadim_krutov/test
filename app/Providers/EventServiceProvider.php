<?php

namespace App\Providers;

use App\Events\CreateUserEvent;
use App\Listeners\AdvertActivitySubscriber;
use App\Listeners\NewUserWelcomeEmail;
use App\Listeners\RequestsSubscriber;
use App\Listeners\SubscribeUserToMailchimp;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        
    ];

    protected $subscribe = [
        
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
