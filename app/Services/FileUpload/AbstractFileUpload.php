<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 09/05/2017
 * Time: 09:16
 */

namespace App\Services\FileUpload;


use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class AbstractFileUpload
{
    abstract protected function upload(UploadedFile $file);
    abstract protected function delete($fileName);

    /**
     * @param $string
     * @param bool $force_lowercase
     * @param bool $anal
     * 
     * @return mixed|string
     */
    protected function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }

    /**
     * @param $filename
     * @param $extension
     *
     * @return string
     */
    protected function createUniqueFilename($filename, $extension)
    {
        $imageToken = substr(sha1(mt_rand()), 0, 5);

        return $filename . '-' . $imageToken . '.' . $extension;
    }
}