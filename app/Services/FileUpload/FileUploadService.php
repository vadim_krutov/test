<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 08/05/2017
 * Time: 17:38
 */

namespace App\Services\FileUpload;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Response;


class FileUploadService extends AbstractFileUpload
{
    private $uploadDir;

    public function __construct()
    {
        $this->uploadDir = public_path(). '/uploads/vacancies';
    }

    public function upload(UploadedFile $file)
    {
        $originalName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);

        $filename = $this->sanitize($originalNameWithoutExt);
        $allowed_filename = $this->createUniqueFilename($filename, $extension);

        $file->move($this->uploadDir, $allowed_filename);

        return $allowed_filename;
    }

    /**
     * Delete Image From Session folder, based on original filename
     *
     * @param $fileName
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($fileName)
    {
        $path = $this->uploadDir . $fileName;

        if (\File::exists($path)) {
            \File::delete($path);
        }

        return true;
    }
}