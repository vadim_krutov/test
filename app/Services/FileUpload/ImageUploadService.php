<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 08/05/2017
 * Time: 17:38
 */

namespace App\Services\FileUpload;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;

class ImageUploadService extends AbstractFileUpload
{
    public function upload(UploadedFile $photo)
    {
        $originalName = $photo->getClientOriginalName();
        $extension = $photo->getClientOriginalExtension();
        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);

        $filename = $this->sanitize($originalNameWithoutExt);
        $allowed_filename = $this->createUniqueFilename($filename, $extension);

        $this->original($photo, $allowed_filename);
        $this->icon($photo, 'thumb_' . $allowed_filename);

        return $allowed_filename;
    }

    /**
     * Optimize Original Image
     *
     * @param $photo
     * @param $filename
     *
     * @return Image
     */
    public function original($photo, $filename)
    {
        $manager = new ImageManager();
        $image = $manager->make($photo)->save(\Config::get('images.full_size') . $filename );

        return $image;
    }

    /**
     * Create Icon From Original
     *
     * @param $photo
     * @param $filename
     * @return Image
     */
    public function icon($photo, $filename)
    {
        $manager = new ImageManager();
        $image = $manager->make( $photo )->resize(200, 200, function ($constraint) {
            $constraint->aspectRatio();
        })
            ->save(\Config::get('images.icon_size')  . $filename );

        return $image;
    }

    /**
     * Delete Image From Session folder, based on original filename
     *
     * @param $fileName
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($fileName)
    {
        $full_size_dir = \Config::get('images.full_size');
        $icon_size_dir = \Config::get('images.icon_size');
        $full_path1 = $full_size_dir . $fileName;
        $full_path2 = $icon_size_dir . $fileName;

        if (\File::exists($full_path1)) {
            \File::delete($full_path1);
        }

        if (\File::exists($full_path2)) {
            \File::delete($full_path2);
        }
    }
}