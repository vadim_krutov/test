<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 12/12/2016
 * Time: 14:45
 */

namespace App\Services;


use App\Entity\Car;
use App\Entity\CarAdvert;
use App\Entity\CarModel;
use App\Entity\Repository\TextRepository;
use App\Entity\Text;
use App\Entity\UserRole;


class InfoService
{
    const DATA_TYPE_FORM_ROLES = 1;
    const DATA_TYPE_GROUPED_TRANSLATIONS = 2;

    /**
     * @param string $groupBy
     *
     * @return array
     */
    public static function getGroupedTranslations(string $groupBy): array
    {
        /** @var TextRepository $repository */
        $repository = \EntityManager::getRepository(Text::class);
        $texts = $repository->findByGroup($groupBy);
        $translations = [];

        /** @var Text $text */
        foreach ($texts as $text) {
            $translations[$text->getId()] = $text->getTranslationByLocale('en')->getDescription();
        }

        return $translations;
    }

    /**
     * @param string $dataType
     *
     * @return array
     */
    public function get(string $dataType): array
    {
        switch ($dataType) {
            case static::DATA_TYPE_FORM_ROLES:
                return $this->formRoles();
                break;
        }

        return [];
    }

    private function formRoles(): array
    {
        $roles = [];

        /** @var UserRole $role */
        foreach (\EntityManager::getRepository(UserRole::class)->findAll() as $role) {
            $roles[$role->getId()] = $role->getName();
        }

        return $roles;
    }
}