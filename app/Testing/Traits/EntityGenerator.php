<?php
/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 05/12/2016
 * Time: 17:54
 */

namespace App\Testing\Traits;


use App\Entity\Request;
use App\Entity\RequestFlight;
use App\Entity\RequestHotel;
use App\Entity\RequestTransfer;
use App\Entity\Subscription;
use App\Entity\User;
use App\Entity\UserRole;
use App\Entity\UserSubscription;

trait EntityGenerator
{
    /**
     * @param string $email
     * @param string $password
     * @param string $role
     * 
     * @return User
     */
    public function generateUser(string $email, string $password, string $role = UserRole::ROLE_USER): User
    {
        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setRole(\EntityManager::getRepository(UserRole::class)->findOneBy(['name' => $role]));

        \EntityManager::persist($user);
        \EntityManager::flush();

        return $user;
    }
}