role :app, %w{lifa.lv}

#require custom config
require './config/myconfig.rb'

namespace :deploy do

components_dir = '/home/lifa/domains/stg.aslbgp.lifa.lv/public_html/components'
set :components_dir, components_dir

  desc 'Copy vendors'
    task :put_components do
      on roles(:app), in: :sequence, wait: 1 do
        system("tar -zcf ./vendor.tar.gz ./vendor ")
        upload! './vendor.tar.gz', "#{components_dir}", :recursive => true
        execute "cd #{components_dir}
        tar -zxf /home/lifa/domains/stg.aslbgp.lifa.lv/public_html/components/vendor.tar.gz"
        system("rm ./vendor.tar.gz")
      end
  end

  desc 'Copy non-git ENV specific files to servers.'
      task :put_env_components do
        on roles(:app), in: :sequence, wait: 2 do
          upload! './.env.staging', "#{deploy_to}/../components/.env"
        end
  end

  desc 'Get stuff ready prior to symlinking'
  task :compile_assets do
    on roles(:app), in: :sequence, wait: 3 do
      execute "cp #{deploy_to}/../components/.env #{release_path}"
      execute "cp -r #{deploy_to}/../components/vendor #{release_path}"
    end
  end

  desc 'Publish ckeditor'
      task :publish_vendors do
        on roles(:app), in: :sequence, wait: 4 do
          execute "cd #{deploy_to}/current && php artisan vendor:publish --tag=ckeditor"
        end
  end

  desc 'Apply database migrations'
    task :apply_db_migrations do
      on roles(:app), in: :sequence, wait: 5 do
        execute "cd #{deploy_to}/current && php artisan doctrine:schema:update --force"
      end
  end

  desc 'Create non-git directories'
      task :create_non_git_dirs do
        on roles(:app), in: :sequence, wait: 5 do
          execute "cd #{deploy_to}/current && mkdir -p public/uploads/images/full_size"
          execute "cd #{deploy_to}/current && mkdir -p public/uploads/images/icon_size"
          execute "cd #{deploy_to}/current && chmod -R 777 public/uploads"
        end
  end

  after :updated, :put_components
  after :put_components, :put_env_components
  after :put_env_components, :compile_assets
  after :log_revision, :apply_db_migrations
  after :log_revision, :publish_vendors
  after :apply_db_migrations, :create_non_git_dirs
end
