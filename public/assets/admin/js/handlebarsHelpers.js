/**
 * Created by vadimkrutov on 03/01/2017.
 */
var renderTemplate = function(templateFileName, objectData, renderContainer, callback) {
    $.get( '/assets/admin/hbs/' + templateFileName + '.handlebars', function(templateResponse) {
        var template = Handlebars.compile(templateResponse);
        
        $(renderContainer).html(template(objectData));
    }).done(callback);
};

var renderPartialTemplate = function(template, jsonData, renderContainer, callback) {
    $.get( '/assets/admin/hbs/' + template + '.handlebars', function(data){
        var template = Handlebars.compile(data);

        $(renderContainer).append(template(jsonData));
    }).done(callback);
};

/** Custom Helper to have if condition with all operators */
Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});