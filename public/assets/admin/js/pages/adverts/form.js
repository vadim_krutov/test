/**
 * Created by vadimkrutov on 06/05/2017.
 */
var getCarModels = function (id) {
    return $.ajax({
        url: '/admin/adverts/car/' + id + '/models',
        type: 'GET',
        success: function (response) {
            return response;
        }
    });
};

var selectCar = $('select.car');
selectCar.change(function () {
    var carId = $(this).val();

    getCarModels(carId).then(function (data) {
        renderTemplate('adverts/_car_model_option', data, 'select.models', function () {
            $('.selectpicker').selectpicker('refresh');
        });
    })
});

setTimeout(function () {
    $('.bootstrap-select ul.dropdown-menu li:first').css('display', 'none');
}, 500);


if (selectCar.find('option:selected').val() != "0") {
    var id = selectCar.find('option:selected').val();

    getCarModels(id).then(function (data) {
        renderTemplate('adverts/_car_model_option', data, 'select.models', function () {
            var id = window.localStorage.getItem('car_model_id');

            if (id == null) {
                id = $('select.models option:selected').val();
            }

            $('select.models').find('option[value="'+id+'"]').attr('selected', 'selected');
            // $('.selectpicker').selectpicker('refresh');
        });
    })
}

$('select.models').change(function () {
    var id = $(this).val();
    window.localStorage.setItem('car_model_id', id);
});

var token = $('input[name="_token"]').val();
var sessionFiles = [];

$.ajaxSetup({
    headers: {
        'X-CSRF-Token': token
    }
});

Dropzone.autoDiscover = false;
var dropZone = new Dropzone('div#myDrop', {
    url: "/admin/files/image",
    addRemoveLinks: true,
    params: {
        _token: token
    },

    init: function () {
        this.on('removedfile', function (file) {
            sessionFiles = sessionFiles.filter(function (image) {
                console.log(image);
                console.log(file.serverName)
                return image.path != file.serverName.path
            });
        });

        this.on('addedfile', function (file) {
            if (file.serverName != undefined) {
                $(file.previewElement).attr('serverName', file.serverName.path);
            }
        });

        $("#myDrop").sortable({
            items:'.dz-preview',
            cursor: 'move',
            opacity: 1,
            containment: '#myDrop',
            distance: 20,
            tolerance: 'pointer',
            stop: function (e, ui) {

            }
        });
    },
    success: function (file, response) {
        sessionFiles.push(response.data);
        file.serverName = response.data;

        $(file.previewElement).attr('serverName', response.data.path);
    }
}) ;


if ($('.advert_id').val() != undefined) {
    var getAdvertPhotos = function () {
        return $.ajax({
            type: 'GET',
            url: '/admin/adverts/' + $('.advert_id').val() + '/photos',
            success: function (res) {
                return res;
            }
        })
    };

    getAdvertPhotos().then(function (res) {
        var existingFiles = [];

        res.data.photos.data.forEach(function (item) {
            var file = {
                name: item.path,
                size: 50000,
                serverName: {
                    path: item.path,
                    thumb: item.thumb
                }
            };

            sessionFiles.push({
                path: item.path,
                thumb: item.thumb,
                order: item.order
            });

            existingFiles.push(file);
        });


        for (var i = 0; i < existingFiles.length; i++) {
            dropZone.emit("addedfile", existingFiles[i]);
            dropZone.emit(
                "thumbnail",
                existingFiles[i],
                "/uploads/advert_images/icon_size/thumb_" + existingFiles[i].name
            );
            dropZone.emit("complete", existingFiles[i]);
        }
    });
}

$('#carForm').submit(function () {
    $('#myDrop .dz-preview').each(function (idx, elem) {
        var serverName = $(elem).attr('serverName');

        sessionFiles.forEach(function (elem) {
            if (elem.path == serverName) {
                elem.order = idx
            }
        });
    });

    sessionFiles.forEach(function (elem, idx) {
        var html = '<div>';
        html += '<input type="hidden" name="photos['+idx+'][path]" value="'+elem.path+'" />';
        html += '<input type="hidden" name="photos['+idx+'][thumb]" value="'+elem.thumb+'" />';
        html += '<input type="hidden" name="photos['+idx+'][order]" value="'+elem.order+'" />';
        html += '</div>';

        $('#carForm').append(html);
    });
});