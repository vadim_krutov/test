/**
 * Created by vadimkrutov on 03/05/2017.
 */
var table = $('.car_model_table');
table.DataTable({
    pageLength: 100
});

$(document).on('click', '.delete__car', function () {
    var id = $(this).data('id');
    var html = '<input class="deleted" type="hidden" name="deleted[]" value="'+id+'" />';

    $('#carForm').append(html);
    $(this).parent().parent().remove();

    return false;
});


$('#popover').popover({
    html : true,
    title: function() {
        return $("#popover-head").html();
    },
    content: function() {
        return $("#popover-content").html();
    }
});

$('body').on('hidden.bs.popover', function (e) {
    $(e.target).data("bs.popover").inState.click = false;
});

$(document).on('click', '.add__car', function () {
    var modelValue = $(this).parent().parent().find('input').val();
    table.DataTable().destroy();

    if (modelValue == "") {
        $(this).parent().parent().find('input').parent().addClass('has-error');

    } else {
        var idx = $('.table tbody tr').length + 1;
        var data = {
            title: modelValue,
            idx: idx
        };

        renderPartialTemplate('cars/_car_model_row', data, 'table tbody', function () {
            table.DataTable({
                pageLength: 100
            });

            $('#popover').popover('hide');
        })
    }
});