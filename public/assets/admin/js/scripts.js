/**
 * Created by vadimkrutov on 09/12/2016.
 */
$('.data__table').DataTable();
$('.datepicker').datepicker({
    autoclose: true,
    format: "dd/mm/yyyy"
});

$(document).on('click', '.delete_item', function () {
    return confirm('Are you sure want delete this item?');
});

$('.nav-item').each(function () {
    var section = $(this).data('section');

    if (section != undefined && window.location.href.indexOf(section) != -1) {
        $('.nav-item').removeClass('active').removeClass('open');
        $(this).addClass('open').addClass('active')
    }

});