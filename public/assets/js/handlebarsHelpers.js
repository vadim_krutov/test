/**
 * Created by vadimkrutov on 03/01/2017.
 */
var renderTemplate = function(templateFileName, objectData, renderContainer, callback) {
    $.get( '/assets/hbs/' + templateFileName + '.handlebars', function(templateResponse) {
        var template = Handlebars.compile(templateResponse);
        
        $(renderContainer).html(template(objectData));
    }).done(callback);
};

var renderPartialTemplate = function(template, jsonData, renderContainer, callback) {
    $.get( '/assets/hbs/' + template + '.handlebars', function(data){
        var template = Handlebars.compile(data);

        $(renderContainer).append(template(jsonData));
    }).done(callback);
};