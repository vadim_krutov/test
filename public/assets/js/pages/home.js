/**
 * Created by vadimkrutov on 21/12/2017.
 */
var airports = [];

//TODO - Implement pre loader while airports data is loading
$.get('/api/rest/airports', function (res) {
    airports = res.data;
});

var inputAirportElem = $('input.airport');

inputAirportElem.keyup(function () {
    var dataContainer = $(this).data('container');
    var searchVal = $(this).val().toUpperCase();
    var _this = $(this);
    inputAirportElem = _this;

    var filtered = airports.filter(function (val) {
        if (val.code.match(searchVal)) {
            return val;
        }
    });

    var renderContainer = 'ul.'+ dataContainer;
    if (searchVal != "") {
        $(renderContainer).parent().show();
        renderTemplate('_airport_entry', filtered, renderContainer, function () {
            $('.airport_item').click(function () {
                _this.val($(this).find('p').text());
                $(renderContainer).parent().hide();
            })
        });
    } else {
        $(renderContainer).parent().hide();
    }
});

inputAirportElem.focusout(function () {
    var dataContainer = $(this).data('container');
    var renderContainer = 'ul.'+ dataContainer;

    setTimeout(function () {
        $(renderContainer).parent().hide();
    }, 100);
});

$('span.schedule').click(function () {
    var dataInput = $(this).data('input');
    var showContainer = $('div.' + dataInput);

    if (showContainer.is(':visible')) {
        showContainer.hide()
    } else {
        showContainer.slideDown();
    }

    $('li.schedule_item').click(function () {
        var val = $(this).find('p').text().split(' ');
        delete val[0];
        val = val.join(' ');

        $('input.' + dataInput).val(val);

        showContainer.hide();
    });
});

$('.btn-search').click(function () {
    var form = $(this).parent().parent().parent();

    form.submit();
});