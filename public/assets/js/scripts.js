/**
 * Created by Saule on 21/10/2016.
 */
$('#equalheight div div.panel-default').equalHeights();

var panelBody = $('.panel-body') ;

panelBody.hover(function () {
    $(this).parent().addClass('panel-shadow')
});

panelBody.mouseleave(function () {
    $(this).parent().removeClass('panel-shadow')
});

$('.datepicker').datetimepicker({
    format: 'DD/MM/YYYY'
});