<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Login</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />

    {{ Html::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') }}
    {{ Html::style('assets/admin/vendor/font-awesome/css/font-awesome.min.css') }}
    {{ Html::style('assets/admin/vendor/simple-line-icons/simple-line-icons.min.css') }}
    {{ Html::style('assets/admin/vendor/bootstrap/css/bootstrap.min.css') }}
    {{ Html::style('assets/admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}
    {{ Html::style('assets/admin/vendor/bootstrap-select/css/bootstrap-select.min.css') }}

    {{ Html::style('assets/admin/css/components-md.min.css') }}
    {{ Html::style('assets/admin/css/plugins-md.min.css') }}

    {{ Html::style('assets/admin/css/layout.min.css') }}
    {{ Html::style('assets/admin/css/themes/darkblue.min.css') }}
    {{ Html::style('assets/admin/css/custom.min.css') }}
    {{ Html::style('assets/admin/css/login-4.css') }}

    @yield('css')
    <link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<body class=" login">
<div class="logo">
    <a href="#">
        {{ Html::image('assets/admin/img/logo-big.png') }}
    </a>
</div>
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ url(route('admin::postLogin')) }}" method="post">
        {{ csrf_field() }}
        <h3 class="form-title">Login to your account</h3>

        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="input-icon">
                <i class="fa fa-at"></i>
                <input class="form-control placeholder-no-fix"
                       placeholder="E-mail"
                       name="email"
                       type="email"
                       autofocus
                       autocomplete="off">
            </div>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control"
                       placeholder="Password"
                       name="password"
                       type="password"
                       value="">
            </div>

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-actions">
            <button type="submit" class="btn green pull-right"> Login </button>
        </div>
    </form>
    <!-- END LOGIN FORM -->
</div>


<!--[if lt IE 9]>
{{ Html::script('assets/admin/vendor/respond.min.js') }}
{{ Html::script('assets/admin/vendor/excanvas.min.js') }}
{{ Html::script('assets/admin/vendor/ie8.fix.min.js') }}
<![endif]-->

<!-- BEGIN CORE PLUGINS -->
{{ Html::script('assets/admin/vendor/jquery.min.js') }}
{{ Html::script('assets/admin/vendor/bootstrap/js/bootstrap.min.js') }}
{{ Html::script('assets/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}
{{ Html::script('assets/admin/vendor/bootstrap-select/js/bootstrap-select.js') }}

<!-- END CORE PLUGINS -->

{{ Html::script('assets/admin/js/app.js')  }}

</body>

</html>