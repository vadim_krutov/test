<div class="col-lg-3 col-md-6">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa {{ $icon }} fa-fw"></i> {{ $title }}
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="list-group">
                @foreach ($items as $item)
                <a href="#" class="list-group-item">
                    {{ method_exists($item, 'getTranslations') ?
                        $item->getTranslations()[0]->getTitle() : $item->getTitle()
                    }}
                </a>
                @endforeach
            </div>
            <!-- /.list-group -->
            @can('access-content', $acl)
                <a href="#" class="btn btn-default btn-block">View All</a>
            @endcan
        </div>
        <!-- /.panel-body -->
    </div>
</div>