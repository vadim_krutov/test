<script src="{{ URL::asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ URL::asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
    $('.ckeditor').ckeditor();

    $('#ckForm').submit(function () {
        $(this).find('.ckeditor').each(function () {
            var data = CKEDITOR.instances[$(this).attr('name')].getData();
            $(this).val(data);
        });
    })
</script>