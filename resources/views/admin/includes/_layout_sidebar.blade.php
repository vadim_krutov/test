<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed "
            data-keep-expanded="false" data-auto-scroll="true"
            data-slide-speed="200"
            style="padding-top: 20px">

            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            @can('access-content', 'page.view')
                <li class="nav-item open" data-section="pages">
                    <a href="javascript:;" class="nav-link nav-toggle" data-prefix="pages">
                        <i class="icon-list"></i>
                        <span class="title">Pages / Page Blocks</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="{{ url(route('admin::page.index')) }}" class="nav-link ">
                                <span class="title">Search Pages</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('access-content', 'photo.edit')
                <li class="nav-item open" data-section="photos">
                    <a href="javascript:;" class="nav-link nav-toggle" data-prefix="photos">
                        <i class="icon-list"></i>
                        <span class="title">Photos</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="{{ url(route('admin::photo.showForm')) }}" class="nav-link ">
                                <span class="title">Manage photos</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('access-content', 'contact.view')
                <li class="nav-item active open" data-section="contacts">
                    <a href="javascript:;" class="nav-link nav-toggle" data-prefix="contacts">
                        <i class="icon-list"></i>
                        <span class="title">Contacts</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="{{ url(route('admin::contact.index')) }}" class="nav-link ">
                                <span class="title">Search Contacts</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('access-content', 'user.view')
                <li class="nav-item" data-section="users">
                    <a href="javascript:;" class="nav-link nav-toggle" data-prefix="users">
                        <i class="icon-user"></i>
                        <span class="title">Users</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="{{ url(route('admin::user.index')) }}" class="nav-link">
                                <span class="title">Search Users</span>
                            </a>
                        </li>
                        @can('access-content', 'role.view')
                        <li class="nav-item  ">
                            <a href="{{ url(route('admin::role.index')) }}" class="nav-link ">
                                <span class="title">Search Roles</span>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @if (Gate::check('access-content', 'text.view') || Gate::check('access-content', 'settings.contacts.edit'))
                <li class="nav-item" data-section="settings">
                    <a href="javascript:;" class="nav-link nav-toggle" data-prefix="settings">
                        <i class="icon-settings"></i>
                        <span class="title">Settings</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        @can('access-content', 'text.view')
                        <li class="nav-item  ">
                            <a href="{{ url(route('admin::text.index')) }}" class="nav-link ">
                                <span class="title">Texts translations</span>
                            </a>
                        </li>
                        @endcan

                    </ul>
                </li>
            @endif
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
