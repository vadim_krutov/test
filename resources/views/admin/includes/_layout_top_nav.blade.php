<ul class="nav navbar-nav pull-right">
    <li class="dropdown dropdown-user">
        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <span class="username username-hide-on-mobile"> {{ Auth::user()->getFullName() }} </span>
        </a>
    </li>
    <li class="dropdown dropdown-quick-sidebar-toggler">
        <a href="{{ url(route('admin::logout')) }}" class="dropdown-toggle">
            <i class="icon-logout"></i>
        </a>
    </li>
</ul>