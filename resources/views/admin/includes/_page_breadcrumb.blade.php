@if (isset($breadcrumbs))
<ul class="page-breadcrumb">
    @foreach ($breadcrumbs as $url => $value)
    <li>
        <a href="{{ $url }}">{{ $value }}</a>
    </li>
    @endforeach
</ul>
@endif