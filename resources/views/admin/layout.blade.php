<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>@yield('title')</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />

    {{ Html::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') }}
    {{ Html::style('assets/admin/vendor/font-awesome/css/font-awesome.min.css') }}
    {{ Html::style('assets/admin/vendor/simple-line-icons/simple-line-icons.min.css') }}
    {{ Html::style('assets/admin/vendor/bootstrap/css/bootstrap.min.css') }}
    {{ Html::style('assets/admin/vendor/bootstrap-switch/css/bootstrap-switch.min.css') }}
    {{ Html::style('assets/admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}
    {{ Html::style('assets/admin/vendor/bootstrap-select/css/bootstrap-select.min.css') }}
    {{ Html::style('assets/admin/vendor/datatables/datatables.min.css') }}
    {{ Html::style('assets/admin/vendor/datatables-plugins/dataTables.bootstrap.css') }}

    {{ Html::style('assets/admin/css/components-md.min.css') }}
    {{ Html::style('assets/admin/css/plugins-md.min.css') }}

    {{ Html::style('assets/admin/css/layout.min.css') }}
    {{ Html::style('assets/admin/css/themes/darkblue.min.css') }}
    {{ Html::style('assets/admin/css/custom.css') }}
    {{--<link rel="shortcut icon" href="favicon.ico" />--}}
    @yield('css')

</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
<div class="page-wrapper">
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <div class="page-logo">
                <a href="">
                    {{ Html::image('assets/admin/img/logo.png', "logo", ['class' => 'logo-default']) }}
                </a>
                <div class="menu-toggler sidebar-toggler">
                    <span></span>
                </div>
            </div>
            <a href="javascript:;"
               class="menu-toggler responsive-toggler"
               data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <div class="top-menu">
                @include('admin.includes._layout_top_nav')
            </div>
        </div>
    </div>
    <div class="clearfix"> </div>
    <div class="page-container">
        @include('admin.includes._layout_sidebar')

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    @include('admin.includes._page_breadcrumb')
                </div>
                <h1 class="page-title"> @yield('title')
                    <small>@yield('sub_title')</small>
                </h1>

                @yield('content')
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="page-footer-inner">
            <a target="_blank" href="http://keenthemes.com"></a> &nbsp;|&nbsp;
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
</div>

<!--[if lt IE 9]>
{{ Html::script('assets/admin/vendor/respond.min.js') }}
{{ Html::script('assets/admin/vendor/excanvas.min.js') }}
{{ Html::script('assets/admin/vendor/ie8.fix.min.js') }}
<![endif]-->

<!-- BEGIN CORE PLUGINS -->
{{ Html::script('assets/admin/vendor/jquery.min.js') }}
{{ Html::script('assets/admin/vendor/bootstrap/js/bootstrap.min.js') }}
{{ Html::script('assets/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}
{{ Html::script('assets/admin/vendor/bootstrap-select/js/bootstrap-select.js') }}
{{ Html::script('assets/admin/vendor/datatables/datatables.min.js') }}
{{ Html::script('assets/admin/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}
{{ Html::script('assets/admin/vendor/bootstrap-select/js/bootstrap-select.js') }}
{{ Html::script('assets/admin/vendor/js.cookie.min.js') }}
{{ Html::script('assets/admin/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}
{{ Html::script('assets/admin/vendor/jquery.blockui.min.js') }}
{{ Html::script('assets/admin/vendor/bootstrap-switch/js/bootstrap-switch.min.js') }}
{{ Html::script('assets/admin/vendor/moment.min.js') }}
{{ Html::script('assets/admin/vendor/handlebars/handlebars-v4.0.5.js') }}
<!-- END CORE PLUGINS -->

{{ Html::script('assets/admin/js/app.js')  }}
{{ Html::script('assets/admin/js/layout.js')  }}
{{ Html::script('assets/admin/js/handlebarsHelpers.js')  }}
{{ Html::script('assets/admin/js/scripts.js')  }}

@yield('js')

</body>

</html>