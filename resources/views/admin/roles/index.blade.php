@extends('admin.layout')

@section('title')
    Roles
@endsection

@section('content')
    <div class="table-toolbar">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-group pull-right">
                    <a href="{{ url(route('admin::role.showForm')) }}" class="btn sbold green"> Add New
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
            @include('admin.includes._form_response_messages')
            <table width="100%" class="table table-striped table-bordered table-hover data__table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr class="odd gradeX">
                        <td>{{ $role->getName() }}</td>
                        <td>{{ $role->getDescription() }}</td>
                        <td class="text-center" width="25%">
                            @if ($role->getName() != \App\Entity\UserRole::ROLE_ADMIN)
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle"
                                        type="button" id="dropdownMenu1"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="true">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @can('access-content', 'role.edit')
                                        <li>
                                            <a href="{{ url(route("admin::role.showForm",
                                            ['roleId' => $role->getId()])) }}">
                                                <i class="fa fa-pencil fa-fw"></i>
                                                Edit
                                            </a>
                                        </li>
                                    @endcan
                                    @can('access-content', 'role.permission.edit')
                                        <li>
                                            <a href="{{url(route('admin::role.edit.permissions',
                                            ['roleId' => $role->getId()]))}}">
                                                <i class="fa fa-pencil fa-fw"></i>
                                                Edit permissions
                                            </a>
                                        </li>
                                    @endcan
                                    @can('access-content', 'role.delete')
                                        <li class="delete__item">
                                            <a class=" delete__item" href="{{url(route('admin::role.delete',
                                            ['roleId' => $role->getId()]))}}">
                                                <i class="fa fa-trash fa-fw"></i>
                                                Delete
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection