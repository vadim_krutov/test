@extends('admin.layout')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $text ? "Update Text" : "New Text" }}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                @include('admin.includes._form_errors')
                @include('admin.includes._form_response_messages')
                {{ Form::open([
                    'method' => 'post',
                    'route' => ['admin::text.post', 'textId' => $text ? $text->getId() : null ],
                    'id' => 'ckForm'
                    ])
                }}
                <div class="form-group">
                    {{ Form::label('slug', 'Slug') }}
                    {{ Form::text('slug', $text ? $text->getSlug() : '', ["class" => "form-control"]) }}
                </div>
                @if ($text)
                    @foreach ($text->getTranslations() as $key => $translation)
                        <div class="form-group">
                            {{ Form::label('description', 'Description ('.$translation->getLocale().')') }}
                            {{ Form::text('translations['.$key.'][description]',
                            $translation->getDescription(),
                            ["class" => "form-control"]) }}
                        </div>
                        {{ Form::hidden('translations['.$key.'][locale]', $translation->getLocale())  }}
                        {{ Form::hidden('translations['.$key.'][id]', $translation->getId())  }}
                    @endforeach
                @else
                    @foreach (config('app.languages') as $key => $language)
                    <div class="form-group">
                        {{ Form::label('description', 'Description ('.$language.')') }}
                        {{ Form::text('translations['.$key.'][description]', null,
                        ["class" => "form-control"]) }}
                    </div>
                    {{ Form::hidden('translations['.$key.'][locale]', $language)  }}
                    @endforeach
                @endif
                <div class="form-group">
                    <a href="{{ url(route('admin::text.index')) }}" class="btn btn-default">Cancel</a>
                    <button type="SUBMIT" class="btn btn-success">Submit</button>
                </div>
                {{ csrf_field() }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection


