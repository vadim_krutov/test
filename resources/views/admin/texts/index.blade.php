@extends('admin.layout')

@section('title')
    Text Translations
@endsection

@section('sub_title')
    Static text translations
@endsection

@section('content')
    <div class="table-toolbar">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-group pull-right">
                    <a href="{{ url(route('admin::text.showForm')) }}" class="btn sbold green"> Add New
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 table-responsive">
            @include('admin.includes._form_response_messages')
            <table width="100%" class="table table-striped table-bordered table-hover data__table">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Slug</th>
                    <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                @foreach($texts as $text)
                    <tr class="odd gradeX">
                        <td>{{ $text->getTranslations()[0]->getDescription() }}</td>
                        <td>{{ $text->getSlug() }}</td>
                        <td class="text-center" style="width: 15%">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle"
                                        type="button" id="dropdownMenu1"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="true">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @can('access-content', 'text.edit')
                                        <li>
                                            <a href="{{ url(route("admin::text.showForm", ['textId' => $text->getId()])) }}">
                                                <i class="fa fa-pencil fa-fw"></i>
                                                Edit
                                            </a>
                                        </li>
                                    @endcan
                                    @can('access-content', 'text.delete')
                                        <li class="delete__item">
                                            <a class="delete__item" href="{{url(route('admin::text.delete',
                                        ['textId' => $text->getId()]))}}">
                                                <i class="fa fa-trash fa-fw"></i>
                                                Delete
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection