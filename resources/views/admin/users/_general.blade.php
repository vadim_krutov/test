{{ Form::open([
    'method' => 'post',
    'route' => ['admin::user.post', 'userId' => $user ? $user->getId() : null ]
    ])
}}
<div class="form-group">
    {{ Form::label('email', 'Email') }}
    @if($user)
        <p class="form-control-static">{{ $user->getEmail() }}</p>
    @else
        {{ Form::text('email', $user ? $user->getEmail() : '', ["class" => "form-control"]) }}
    @endif
</div>
<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', $user ? $user->getName() : '', ["class" => "form-control"]) }}
</div>
<div class="form-group">
    {{ Form::label('lastname', 'Lastname') }}
    {{ Form::text('lastname', $user ? $user->getLastname() : '', ["class" => "form-control"]) }}
</div>
<div class="form-group">
    {{ Form::label('mobilePhone', 'Mobile phone') }}
    {{ Form::text('mobilePhone',
        $user ? $user->getMobilePhone() : '', ["class" => "form-control"])
    }}
</div>
<div class="form-group">
    {{ Form::label('role', 'Role') }}
    {{ Form::select('role', $roles,
            $user && $user->getRole() ? $user->getRole()->getId() : '',
            ['class' => 'form-control selectpicker']
        )
    }}
</div>

<div class="form-group">
    {{ Form::label('isBlocked', 'Is Blocked?') }}
    {{ Form::select('isBlocked', ["1" => "Yes", "0" => "No"],
           $user && $user->getIsBlocked() === true ? "1" : "0" ,
           ['class' => 'form-control selectpicker']
       )
   }}
</div>
@if (!$user)
    <div class="form-group">
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password', ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('passwordConfirm', 'Confirm Password') }}
        {{ Form::password('passwordConfirm', ['class' => 'form-control']) }}
    </div>
@endif

<div class="form-group">
    <a href="{{ url(route('admin::user.index')) }}" class="btn btn-default">Cancel</a>
    <button type="SUBMIT" class="btn btn-success">Submit</button>
</div>
{{ csrf_field() }}
{{ Form::close() }}