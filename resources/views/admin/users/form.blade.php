@extends('admin.layout')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $user ? "Manage User" : "New User" }}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                @include('admin.includes._form_errors')
                @include('admin.includes._form_response_messages')
                @include('admin.users._general')
            </div>
        </div>
    </div>
@endsection

