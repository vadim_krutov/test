@extends('admin.layout')

@section('title')
    Users
@endsection

@section('content')
    <div class="table-toolbar">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-group pull-right">
                    <a href="{{ url(route('admin::user.showForm')) }}" class="btn sbold green"> Add New
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 table-responsive">
            @include('admin.includes._form_response_messages')
            <table width="100%" class="table table-striped table-bordered table-hover data__table">
                <thead>
                <tr>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Mobile phone</th>
                    <th>Role</th>
                    <th width="14%">Manage</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr class="odd gradeX">
                        <td>{{ $user->getName() }} {{ $user->getLastName() }}</td>
                        <td>{{ $user->getEmail() }}</td>
                        <td>{{ $user->getMobilePhone() }}</td>
                        <td>{{ $user->getRole() ? $user->getRole()->getName()  : "" }}</td>
                        <td class="text-center" width="14%">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle"
                                        type="button" id="dropdownMenu1"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="true">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    @can('access-content', 'user.edit')
                                        <li>
                                            <a href="{{ url(route('admin::user.showForm',
                                            ['userId' => $user->getId()])) }}">
                                                <i class="fa fa-pencil fa-fw"></i>
                                                Edit
                                            </a>
                                        </li>
                                    @endcan
                                    @can('access-content', 'user.delete')
                                        <li class="delete__item">
                                            <a class="delete__item" href="{{ url(route('admin::user.delete',
                                               ['userId' => $user->getId()])) }}">
                                                <i class="fa fa-trash fa-fw"></i>
                                                Delete
                                            </a>
                                        </li>
                                    @endcan
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection