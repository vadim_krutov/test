<!-- Departure -->
<div role="tabpanel"
     class="tab-pane {{ !isset($filters) ? "active" : "" }} {{ isset($filters) && $filters['type'] == "departures" ? "active" : "" }}"
     id="home">
    <form method="post" action="{{ url(route('home::showResults')) }}">
        <div class="departure">
            <p>Departure:</p>
            <div class="input-group dep-input">
                <span class="input-group-addon">
                    <img src="{{ asset('assets/images/departure-icon.png') }}"
                         alt="can't load an image. Please empty your cache and try to reload a page">
                </span>
                <input type="text"
                       autocomplete="off"
                       name="filters[departure]"
                       class="form-control airport"
                       aria-describedby="basic-addon1"
                       value="{{ isset($filters) && $filters['type'] == "departures" ? $filters['departure'] : "" }}"
                       data-container="departure_departure" />
            </div>
            <div class="dep-input-results-panel" style="display: none;">
                <ul class="results-list-container airports-container departure_departure">

                </ul>
            </div>
        </div>
        <div class="destination">
            <p>Destination:</p>
            <div class="input-group dest-input">
                <span class="input-group-addon">
                    <img src="{{ asset('assets/images/destination-icon.png') }}"
                         alt="can't load an image. Please empty your cache and try to reload a page">
                </span>
                <input type="text"
                       autocomplete="off"
                       name="filters[arrival]"
                       class="form-control airport"
                       aria-describedby="basic-addon1"
                       value="{{ isset($filters) && $filters['type'] == "departures" ? $filters['arrival'] : "" }}"
                       data-container="departure_destination" />
            </div>

            <div class="dest-input-results-panel" style="display: none;">
                <ul class="results-list-container airports-container departure_destination">

                </ul>
            </div>
        </div>
        <div class="date">
            <p>Departure Date:</p>
            <div class="input-group date-input">
                <span class="input-group-addon">
                     <img src="{{ asset('assets/images/date-icon.png') }}"
                          alt="can't load an image. Please empty your cache and try to reload a page">
                </span>
                <input type="text"
                       autocomplete="off"
                       name="filters[date]"
                       value="{{ isset($filters) && $filters['type'] == "departures" ? $filters['date'] : "" }}"
                       class="form-control datepicker"
                       aria-describedby="basic-addon1">
            </div>
        </div>
        <div class="schedule">
            <p>Schedule:</p>
            <div class="input-group sched-input">
                <span class="input-group-addon">
                    <img src="{{ asset('assets/images/schedule-icon.png') }}"
                         alt="can't load an image. Please empty your cache and try to reload a page">
                </span>
                <input type="text"
                       readonly
                       autocomplete="off"
                       name="filters[schedule]"
                       class="form-control departure_schedule schedule"
                       value="{{ isset($filters) && $filters['type'] == "departures" ? $filters['schedule'] : "" }}"
                       aria-describedby="basic-addon1" />
                <span class="input-group-addon dropdown-arrow-cnt schedule"
                      data-input="departure_schedule">
                    <img src="{{ asset('assets/images/dropdown-arrow.png') }}"
                         alt="can't load an image. Please empty your cache and try to reload a page">
                </span>
            </div>
            <div class="sched-input-results-panel departure_schedule" style="display: none">
                <ul class="results-list-container" data-input="departure_schedule">
                    <li class="schedule_item">
                        <p>FROM 00:00 to 06:00</p>
                    </li>
                    <li class="schedule_item">
                        <p>FROM 06:00 to 12:00</p>
                    </li>
                    <li class="schedule_item">
                        <p>FROM 12:00 to 18:00</p>
                    </li>
                    <li class="schedule_item">
                        <p>FROM 18:00 to 00:00</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="button-search">
            </br>
            <div class="input-group button-search-input">
                <a href="#" class="btn btn-search" type="button">search</a>
            </div>
        </div>
        <input type="hidden" name="filters[type]" value="departures" />
        {{ csrf_field() }}
    </form>
</div>
<!-- End Departure -->