<ul class="nav nav-tabs" role="tablist">
    <li role="presentation"
        class="{{ !isset($filters) ? "active" : "" }} {{ isset($filters) && $filters['type'] == "departures"
        ? "active" : "" }} departures-cnt">
        <a class="tablist-items" href="#home" aria-controls="home" role="tab" data-toggle="tab">Departures</a>
    </li>
    <li role="presentation"
        class="{{ isset($filters) && $filters['type'] == "arrivals" ? "active" : "" }} arrivals-cnt">
        <a class="tablist-items" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Arrivals</a>
    </li>
</ul>