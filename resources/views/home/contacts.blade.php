@extends('layout')

@section('logo')
    <img src="{{ asset('assets/images/logo-main-page.png') }}"
         alt="can't load an image. Please empty your cache and try to reload a page">
@endsection

@section('wrapper-style')
    contacts-nav-content-wrapper
@endsection

@section('additional-content')
    <section class="contacts-section">
        <div class="row contacts-container">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1 contacts-title-cnt">
                <p class="contacts-title">Meet Our Team</p>
            </div>
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1 contacts-content-wrapper">
                <div class="row category-container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 category-title-cnt">
                        <p class="category-title">Sales department</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 contact-wrapper">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-2 col-xs-offset-5 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 contacts-image-container"></div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 contacts-information-container">
                            <p class="contacts-name">
                                <span>HILDRETH</span> Todd
                            </p>
                            <p class="contacts-position">
                                Commercial Director
                            </p>
                            <a class="contacts-phone-number" type="text" href="tel:+3242393103‬">‭+32 4 239 31 03‬</a>
                            <a class="contacts-email" type="text" href="mailto:thildreth@aslairlines.com">
                                thildreth@aslairlines.com
                            </a>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 contact-wrapper">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-2 col-xs-offset-5 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 contacts-image-container"></div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 contacts-information-container">
                            <p class="contacts-name">
                                <span>RAETS</span> Sandrine
                            </p>
                            <p class="contacts-position">
                                Commercial Manager
                            </p>
                            <a class="contacts-phone-number" type="text" href="tel:+3242393110‬">+32 4 239 31 10‬</a>
                            <a class="contacts-email" type="text" href="mailto:sraets@aslairlines.com">
                                sraets@aslairlines.com
                            </a>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    <section class="company-contact-section">
        <div class="row">
            <div class="col-lg-12 cl-md-12 col-sm-12 email-icon-wrapper">
                <img src="{{ asset('assets/images/email-icon.png') }}" class="email-icon"
                     alt="can't load an image. Please empty your cache and try to reload a page">
            </div>
        </div>
        <div class="row email-address-container">
            <div class="col-lg-12 col-md-12 col-sm-12 email-icon-wrapper">
                <p class="company-address">ASL Airlines Belgium</br>
                    Rue de l'Aéroport 101</br>
                    4460 Grâce-Hollogne</br>
                    Belgium</p>
            </div>
        </div>
    </section>
    <footer>
        <div class="signature-line"></div>
    </footer>
@endsection

@section('js')
    <script src="{{ asset('assets/js/ekko-lightbox.min.js') }}"></script>
    <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>
@endsection