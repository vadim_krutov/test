@extends('layout')

@section('logo')
    <img src="{{ asset('assets/images/logo_gallery.png') }}" class="navigation-logo"
         alt="can't load an image. Please empty your cache and try to reload a page">
@endsection

@section('css')
    <link href="{{ asset('assets/css/ekko-lightbox.css') }}" rel="stylesheet">
@endsection

@section('wrapper-style')
    gallery-nav-content-wrapper
@endsection

@section('additional-content')
    <section class="gallery-section">
        <div class="row gallery-container">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1 gallery-title-cnt">
                <p class="gallery-title">Photo Gallery</p>
            </div>
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1 gallery-content-wrapper">
                <div class="row justify-content-center">
                    @foreach($photos as $chunk)
                        <div class="row gallery-side-space">
                            @foreach($chunk as $item)
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3 gallery-image-container">
                                    <a href="{{ asset('uploads/images/full_size/' . $item->getFileUrl()) }}"
                                       data-toggle="lightbox" data-gallery="example-gallery" class="image-wrapper-link">
                                        <img src="{{ asset('uploads/images/full_size/' . $item->getFileUrl()) }}"
                                             class="img-fluid" alt="can't load an image. Please empty your cache and try to reload a page">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    <footer>
        <div class="signature-line"></div>
    </footer>
@endsection

@section('js')
    <script src="{{ asset('assets/js/ekko-lightbox.min.js') }}"></script>
    <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>
@endsection