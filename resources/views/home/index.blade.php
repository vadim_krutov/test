@extends('layout')

@section('logo')
    <img src="{{ asset('assets/images/logo-main-page.png') }}"
         alt="can't load an image. Please empty your cache and try to reload a page">
@endsection

@section('wrapper-style')
    content-wrapper
@endsection

@section('content')
    <section>
        <div class="row search-panel-container">
            <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 search-panel-wrapper">
                <div class="search-panel">
                    <!-- Nav tabs -->
                    @include('home._nav_tabs')
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @include('home._departures')
                        @include('home._arrivals')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
