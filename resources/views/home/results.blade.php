@extends('layout')

@section('logo')
    <img src="{{ asset('assets/images/logo-main-page.png') }}"
         alt="can't load an image. Please empty your cache and try to reload a page">
@endsection

@section('wrapper-style')
    results-content-wrapper
@endsection

@section('content')
    <section class="results-page-search-panel-section">
        <div class="row results-search-panel-container">
            <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 search-panel-wrapper">
                <div class="search-panel">
                    <!-- Nav tabs -->
                    @include('home._nav_tabs')
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @include('home._departures')
                        @include('home._arrivals')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('additional-content')
    <section class="results-section">
        <div class="row results-container">

            @if (count($flights) >= 1)
                <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1 results-title-cnt">
                    <p class="results-title">{{ ucfirst($filters['type']) }} Results</p>
                    <p class="results-amount">Found: <span>{{ count($flights) }} flights</span></p>
                </div>
            @else
                <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1 no-result-cnt" style="display: block">
                    <p class="no-results">There is no results found.</br>
                        Please try to enter another criteria.</p>
                </div>
            @endif

            @foreach ($flights as $flight)
                    <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1 result-cnt-one">
                        <div class="row dep-row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-position">
                                <p class="result-title">{{ $flight->getFlightNumber() }}</p>
                            </div>
                            <div class="col-xs-12 hidden-lg hidden-md hidden-sm right-position">
                                <img src="{{ getFlightStatusImage($flight->getStatus()) }}"
                                     alt="can't load an image. Please empty your cache and try to reload a page">
                                <h5 class="result-title">{{ $flight->getStatus() }}</h5>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-12 left-position">
                                <p class="result-sub-title-dep">Departure:</p>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 left-position">
                                <p class="result-airport-dep">{{ $flight->getDepartureAirport() }}</p>
                                <p class="result-date-dep">{{ $flight->getDepartureDate()->format('d/M/Y') }}</p>
                                <p class="result-time-dep">{{ $flight->getDepartureDate()->format('H:i') }}</p>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs right-position">
                                <img src="{{ getFlightStatusImage($flight->getStatus()) }}"
                                     alt="can't load an image. Please empty your cache and try to reload a page">
                                <h4 class="result-title">{{ $flight->getStatus() }}</h4>
                            </div>
                        </div>
                        <div class="row arr-row">
                            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-12 left-position">
                                <p class="result-sub-title-arr">Arrival:</p>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-10 col-xs-12 left-position">
                                <p class="result-airport-arr">{{ $flight->getArrivalAirport() }}</p>
                                <p class="result-date-arr">{{ $flight->getArrivalDate()->format('d/M/Y') }}</p>
                                <p class="result-time-arr">{{ $flight->getArrivalDate()->format('H:i') }}</p>
                            </div>
                        </div>
                    </div>
            @endforeach
        </div>
    </section>
    <footer>
        <div class="signature-line"></div>
    </footer>
@endsection