<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Grand Public</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style-adds.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    @yield('css')
</head>
<body>
<div class="@yield('wrapper-style')">
    <!-- Navigation -->
    <div class="row nav-container">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ url(route('home::index')) }}">
                            @yield('logo')
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="{{ \Route::currentRouteName() == "home::index" ? "active" : "" }}">
                                <a href="{{ url(route('home::index')) }}">Flights
                                    <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="{{ \Route::currentRouteName() == "home::showGallery" ? "active" : "" }}">
                                <a href="{{ url(route('home::showGallery')) }}">Gallery</a>
                            </li>
                            <li class="{{ \Route::currentRouteName() == "home::showContacts" ? "active" : "" }}">
                                <a href="{{ url(route('home::showContacts')) }}">Contacts</a>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    <!-- End Navigation -->
    @yield('content')
</div>
@yield('additional-content')
@yield('footer')

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('assets/js/jquery.min.js')  }}"></script>
<script src="{{ asset('assets/js/jquery.equalheights.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script src="{{ asset('assets/js/handlebars-v4.0.5.js') }}"></script>
<script src="{{ asset('assets/js/handlebarsHelpers.js') }}"></script>
<script src="{{ asset('assets/js/pages/home.js') }}"></script>
@yield('js')
</body>
</html>