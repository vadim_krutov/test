<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/privacy_statement', function () {
    return view('sub.privacy_statement');
})->name('privacyStatement');
Route::get('/terms_of_use', function () {
    return view('sub.terms_of_use');
})->name('termsOfUse');

Route::group(['as' => 'home::'], function (){
    Route::get('/', ['uses' => 'HomeController@index', 'as' => 'index']);
    Route::post('/results', ['uses' => 'HomeController@showResults', 'as' => 'showResults']);
    Route::get('/gallery', ['uses' => 'HomeController@showGallery', 'as' => 'showGallery']);
    Route::get('/contacts', ['uses' => 'HomeController@showContacts', 'as' => 'showContacts']);
});

//Admin routes
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin::'], function (){
    Route::get('login', ['uses' => 'Auth\LoginController@showLoginForm', 'as' => 'showLoginForm']);
    Route::post('login', ['uses' => 'Auth\LoginController@postLogin', 'as' => 'postLogin']);
    Route::get('logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'logout']);

    Route::group(['middleware' => ['auth:admin']], function() {
        Route::get('/', ['uses' => 'UsersController@index', 'as' => 'index', 'permissions' => ['user.view']]);
        //User routes
        Route::group(['prefix' => 'users', 'as' => 'user.'], function (){
            Route::get('/', ['uses' => 'UsersController@index', 'as' => 'index', 'permissions' => ['user.view']]);
            Route::get('/form/{userId?}',
                ['uses' => 'UsersController@showForm', 'as' => 'showForm', 'permissions' => ['user.edit']]
            );
            Route::post('/{userId?}',
                ['uses' => 'UsersController@postUser', 'as' => 'post', 'permissions' => ['user.edit']]
            );
            Route::post('/{userId}/subscription',
                [
                    'uses' => 'UsersController@postUserSubscription', 
                    'as' => 'post.subscription', 
                    'permissions' => ['user.edit']
                ]
            );
            Route::get('/delete/{userId}',
                ['uses' => 'UsersController@deleteUser', 'as' => 'delete', 'permissions' => ['user.delete']]
            );
        });
        //Roles routes
        Route::group(['prefix' => 'roles', 'as' => 'role.'], function (){
            Route::get('/', ['uses' => 'RolesController@index', 'as' => 'index', 'permissions' => ['role.view']]);
            Route::get('/form/{roleId?}',
                ['uses' => 'RolesController@showForm', 'as' => 'showForm', 'permissions' => ['role.edit']]
            );
            Route::post('/{roleId?}',
                ['uses' => 'RolesController@postRole', 'as' => 'post', 'permissions' => ['role.edit']]
            );
            Route::get('/delete/{roleId}',
                ['uses' => 'RolesController@deleteRole', 'as' => 'delete', 'permissions' => ['role.delete']]
            );
            //Roles permissions
            Route::get('/permissions/{roleId}',
                [
                    'uses' => 'RolesController@editRolePermissions',
                    'as' => 'edit.permissions',
                    'permissions' => ['role.permission.edit']
                ]
            );
            Route::post('/{roleId}/permissions',
                [
                    'uses' => 'RolesController@postRolePermissions',
                    'as' => 'post.permissions',
                    'permissions' => ['role.permission.edit']
                ]
            );
        });
        //Texts routes
        Route::group(['prefix' => 'texts','as' => 'text.'], function (){
            Route::get('/', ['uses' => 'TextsController@index', 'as' => 'index', 'permissions' => ['text.view']]);
            Route::get('/form/{textId?}',
                ['uses' => 'TextsController@showForm', 'as' => 'showForm', 'permissions' => ['text.edit']]
            );
            Route::post('/{textId?}',
                ['uses' => 'TextsController@postText', 'as' => 'post', 'permissions' => ['text.edit']]
            );
            Route::get('/delete/{textId}',
                ['uses' => 'TextsController@deleteText', 'as' => 'delete', 'permissions' => ['text.delete']]
            );
        });
    });
});

