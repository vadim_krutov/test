<?php

/**
 * Created by PhpStorm.
 * User: vadimkrutov
 * Date: 05/12/2016
 * Time: 17:52
 */

use \App\Entity\User;

class UserTest extends TestCase
{
    public function testCreateUser()
    {
        /** @var User $user */
        $user = $this->generateUser('test@krutov.lv', 'test123');

        $this->assertDatabaseHas('user', [
            'id' => $user->getId(),
            'email' => $user->getEmail()
        ]);
    }

    public function testCreateAdmin()
    {
        /** @var User $user */
        $user = $this->generateUser('vadim@krutov.lv', 'test123', \App\Entity\UserRole::ROLE_ADMIN);

        /** @var \App\Entity\UserRole $role */
        $role = \EntityManager::getRepository(\App\Entity\UserRole::class)->findOneBy([
            'name' => $user->getRole()->getName()
        ]);

        $this->assertDatabaseHas('user', [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'role_id' => $role->getId()
        ]);
    }
}
